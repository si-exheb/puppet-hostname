# @summary Wrapper class for the module
#
# Set the pretty hostname and control the /etc/hostname file
#
# @example
#   include hostname
class hostname {
  include hostname::pretty

  file { '/etc/hostname':
    ensure => file,
    content => epp('hostname/hostname.epp')
  }
}
