# @summary Set the Pretty Hostname
#
# First look in Hiera, then extract it from the fqdn
#
# @example
#   include hostname::pretty
class hostname::pretty {
  $pretty = lookup('hostname::pretty', String, 'first', 'undef')

  if $pretty == 'undef' {
    exec {}
    notify { 'FQDN':
      name    => 'FQDN',
      message => $facts['hostname']
    }
  } else {
    notify { 'Hiera':
      name    => 'Hiera',
      message => $pretty
    }
  }
}
